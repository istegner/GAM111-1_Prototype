﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    public int playerScore;

    public Text scoreNumber;
    public Text winScoreNumber;

    public string scoreListFileName;
    string fullPath;
    public ScoreList scoreList = new ScoreList();

    void Start()
    {
        //The full path including the file
        fullPath = System.IO.Path.Combine(Application.persistentDataPath, scoreListFileName);
        //Check if the file exists, if not, create it
        if (!System.IO.File.Exists(fullPath))
        {
            System.IO.File.Create(fullPath);
        }
        Debug.Log("ScoreManager Start");
        if (SceneManager.GetActiveScene().name == "Scene1")
        {
            playerScore = 0;
        }
    }

    void Update()
    {
        scoreNumber.text = "Score: " + playerScore.ToString();
    }

    public void AddScore(int score)
    {
        playerScore += score;
    }

    //Save the scorelist
    [ContextMenu("Save")]
    public void SaveNow()
    {
        scoreList.Save(fullPath);
    }

    //Load the scorelist
    [ContextMenu("Load")]
    public void LoadNow()
    {
        scoreList.Load(fullPath);
    }

    [ContextMenu("Trim")]
    public void Trim()
    {
        scoreList.SortAndTrim();
    }
}
