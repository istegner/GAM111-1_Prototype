﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] enemy;
    public List<GameObject> enemiesLeft;

    public Transform[] eSP;

    public int waveNo, totalEnemies, multiplier, maxTimesToTrigger, currentTriggerCount;

    public bool canSpawn;

    public float delay = 0.2f;

    public GameObject winCanvas;
    public GameObject uiCanvas;
    public Text enemiesRemainingText;

    // Use this for initialization
    void Start()
    {
        waveNo = 1;
        totalEnemies = waveNo * multiplier;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Curretn Trigger Count: " + currentTriggerCount);
        enemiesLeft.Clear();
        enemiesLeft.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));
        enemiesLeft.AddRange(GameObject.FindGameObjectsWithTag("EnemyShoot"));
        enemiesLeft.AddRange(GameObject.FindGameObjectsWithTag("EnemyMelee"));
        enemiesRemainingText.text = "Enemies Remaining: " + enemiesLeft.Count.ToString();
        if (enemiesLeft.Count <= 0 && currentTriggerCount < maxTimesToTrigger && canSpawn)
        {
            currentTriggerCount++;
            StartCoroutine(Delay(delay));
        }
        else if (currentTriggerCount >= maxTimesToTrigger)
        {
            Win();
        }
    }

    public void Spawn()
    {
        waveNo++;
        totalEnemies = waveNo * multiplier;
        for (int i = 0; i < totalEnemies; i++)
        {
            Instantiate(enemy[Random.Range(0, enemy.Length)], eSP[Random.Range(0, eSP.Length)]);
        }
    }

    IEnumerator Delay(float delay)
    {
        Debug.Log("Spawn");
        Spawn();
        canSpawn = false;
        yield return new WaitForSeconds(delay);
        canSpawn = true;
    }

    public void Win()
    {
        Time.timeScale = 0f;
        winCanvas.SetActive(true);
        uiCanvas.SetActive(false);
    }
}
