﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekingBulletScontroller : MonoBehaviour
{
    List<GameObject> enemies;

    public Rigidbody2D rb;

    public GameObject bulletSpawn;

    public int speed;

    public GameObject scoreManager;

    // Use this for initialization
    void Start()
    {
        enemies.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));
        enemies.AddRange(GameObject.FindGameObjectsWithTag("EnemyShoot"));
        enemies.AddRange(GameObject.FindGameObjectsWithTag("EnemyMelee"));
        rb = this.GetComponent<Rigidbody2D>();
        bulletSpawn = GameObject.Find("BulletSpawn");
        transform.eulerAngles = bulletSpawn.transform.rotation.eulerAngles;
        Destroy(this.gameObject, 60.0f);
        rb.velocity = new Vector2(GetClosestEnemy(ref enemies).position.x * speed, GetClosestEnemy(ref enemies).position.y * speed);
    }

    // Update is called once per frame
    void Update()
    {

    }

    Transform GetClosestEnemy(ref List<GameObject> enemies)
    {
        Transform tMin = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;
        foreach (GameObject t in enemies)
        {
            float dist = Vector3.Distance(t.transform.position, currentPos);
            if (dist < minDist)
            {
                tMin = t.transform;
                minDist = dist;
            }
        }
        return tMin;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Enemy")
        {
            scoreManager.GetComponent<ScoreManager>().AddScore(100);
            Debug.Log("Hit");
            Destroy(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
