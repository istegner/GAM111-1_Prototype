﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerInput : ScriptableObject
{

    public float xVel, yVel, xFire, yFire;

    public Vector2 MoveVector()
    {
        xVel = Input.GetAxis("Horizontal");
        yVel = Input.GetAxis("Vertical");
        var moveVec = new Vector2(xVel, yVel);
        //Debug.Log(moveVec);
        return moveVec;
    }

    public Vector3 ShootVector()
    {
        xFire = Input.GetAxis("xShoot");
        yFire = Input.GetAxis("yShoot");
        float angle = Mathf.Atan2(xFire, yFire) * Mathf.Rad2Deg;
        Debug.Log(angle);
        var shootVec = new Vector3(0,0,angle);
        return shootVec;
    }
}
