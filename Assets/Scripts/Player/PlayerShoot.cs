﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public GameObject bullet;
    public GameObject seekingBullet;

    public bool shooting;

    public void Shoot(Transform bulletSpawn, string weapon)
    {
        if (weapon == "Single")
        {
            Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation);
        }
        else if (weapon == "Burst")
        {
            StartCoroutine(BurstShoot(0.2f, bulletSpawn));
        }
        else if (weapon == "Seeking")
        {
            Instantiate(seekingBullet, bulletSpawn.position, bulletSpawn.rotation);
        }
    }

    IEnumerator BurstShoot(float delay, Transform bulletSpawn)
    {
        shooting = true;
        Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation);
        yield return new WaitForSeconds(delay);
        Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation);
        yield return new WaitForSeconds(delay);
        Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation);
        yield return new WaitForSeconds(delay);
        shooting = false;
    }
}
