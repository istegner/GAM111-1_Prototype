﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public PlayerInput playerInput;
    public PlayerShoot playerShoot;
    public ScoreManager scoreManager;

    public Transform playfieldOrigin;

    public Vector3 spriteForward = Vector3.right;

    public int speed, health;

    public float shotDelay;

    Rigidbody2D rb;

    public GameObject bulletSpawn;

    public bool canShoot, useMouse;

    public SimpleHealthBar healthBar;

    public string weapon;

    // Use this for initialization
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        var option = File.ReadAllText(Path.Combine(Application.persistentDataPath, "options.txt"));
        if (option == "0")
        {
            useMouse = true;
        }
        else
        {
            useMouse = false;
        }

        health = 100;

        weapon = "Single";
    }

    // Update is called once per frame
    void Update()
    {
        if (!PauseMenu.gameIsPaused)
        {
            if (useMouse)
            {
                //where should I shoot
                var mouseScrPos = Input.mousePosition;
                var mouseRay = Camera.main.ScreenPointToRay(mouseScrPos);
                var playfieldPlane = new Plane(playfieldOrigin.up, playfieldOrigin.position);

                //use ray against playfield to determine where player would shoot
                float rayHitDist = -1;
                if (playfieldPlane.Raycast(mouseRay, out rayHitDist))
                {
                    var rayHitPos = mouseRay.GetPoint(rayHitDist);

                    Debug.DrawLine(transform.position, rayHitPos, Color.black);

                    //now we force this to look at the aim pos
                    transform.rotation = Quaternion.FromToRotation(spriteForward, (rayHitPos - transform.position).normalized);
                }
                if (Input.GetButtonDown("Fire1"))
                {
                    playerShoot.Shoot(bulletSpawn.transform, weapon);
                }
            }
            else
            {
                //Set the rotation of the player
                transform.rotation = Quaternion.Euler(playerInput.ShootVector());
                if ((Input.GetAxis("ControllerShoot") > 0) && (canShoot))
                {
                    canShoot = false;
                    playerShoot.Shoot(bulletSpawn.transform, weapon);
                    StartCoroutine(DelayReset());
                }
            }
            //Set the velocity of the player
            rb.velocity = playerInput.MoveVector() * speed;
        }
    }

    IEnumerator DelayReset()
    {
        yield return new WaitForSeconds(shotDelay);
        canShoot = true;
    }

    /// <summary>
    /// Load the death scene
    /// </summary>
    void Die()
    {
        SceneManager.LoadScene("Death");
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "EnemyBullet")
        {
            health -= 10;
            healthBar.UpdateBar(health, 100);
            if (health <= 0)
            {
                Die();
            }
        }
        if (collision.gameObject.tag == "EnemyMelee")
        {
            Die();
        }
        if (collision.gameObject.tag == "Health")
        {
            health += 50;
            healthBar.UpdateBar(health, 100);
        }
    }
}
