﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    public float delay;
    public bool canShoot = true;

    public void MoveShoot(float speed, GameObject enemy, Transform player, float range, GameObject bullet, GameObject bulletSpawn)
    {
        var distance = Vector2.Distance(enemy.transform.position, player.position);
        if (distance > range)
        {
            float step = speed * Time.deltaTime;
            enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, player.position, step);
        }
        else
        {
            if (canShoot)
            {
                Instantiate(bullet, bulletSpawn.transform.position, bulletSpawn.transform.rotation);
                StartCoroutine(Shoot());
            }
        }
    }

    public void MoveMelee(float speed, GameObject enemy, Transform player, float range)
    {
        var distance = Vector2.Distance(enemy.transform.position, player.position);
        if (distance > range)
        {
            float step = speed * Time.deltaTime;
            enemy.transform.position = Vector2.MoveTowards(enemy.transform.position, player.position, step);
        }
        else
        {
            //Knife the player
        }
    }

    public void Move(float speed, GameObject enemy, Transform player)
    {
        float step = speed * Time.deltaTime;
        enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, player.position, step);
    }

    IEnumerator Shoot()
    {
        canShoot = false;
        yield return new WaitForSeconds(delay);
        canShoot = true;
    }
}
