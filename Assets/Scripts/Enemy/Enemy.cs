﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int shootRange, meleeRange, health, speed;

    private Transform playerTransform;

    public Vector3 spriteForward = Vector3.right;

    public EnemyMove enemyMove;

    public GameObject bullet;
    public GameObject bulletSpawn;

    public Transform PlayerTransform
    {
        get
        {
            if (playerTransform == null)
            {
                var res = GameObject.FindGameObjectWithTag("Player");
                if (res != null)
                {
                    playerTransform = res.transform;
                }
            }

            return playerTransform;
        }
    }

    // Use this for initialization
    void Start()
    {
        health = 100;
    }

    // Update is called once per frame
    void Update()
    {
        var difference = PlayerTransform.position - transform.position;
        var fireDirection = difference.normalized;
        Debug.DrawRay(transform.position, fireDirection * shootRange, Color.red);
        transform.rotation = Quaternion.FromToRotation(spriteForward, fireDirection);

        if (health <= 0)
        {
            Die();
        }

        if (this.gameObject.tag == "EnemyShoot")
        {
            enemyMove.MoveShoot(speed, this.gameObject, playerTransform, shootRange, bullet, bulletSpawn);
        }
        else if (this.gameObject.tag == "EnemyMelee")
        {
            enemyMove.MoveMelee(speed, this.gameObject, playerTransform, meleeRange);
        }
        else
        {
            enemyMove.Move(speed, this.gameObject, playerTransform);
        }
    }

    /// <summary>
    /// Kill the enemy
    /// </summary>
    void Die()
    {
        Destroy(this.gameObject);
    }
}
