﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreTest : MonoBehaviour
{

    public string scoreListFileName;
    string fullPath;
    public ScoreList scoreList = new ScoreList();

    // Use this for initialization
    void Start()
    {
        //The full path including the file
        fullPath = System.IO.Path.Combine(Application.persistentDataPath, scoreListFileName);
        //Check if the file exists, if not, create it
        if(!System.IO.File.Exists(fullPath))
        {
            System.IO.File.Create(fullPath);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Save the scorelist
    [ContextMenu("Save")]
    public void SaveNow()
    {
        scoreList.Save(fullPath);
    }

    //Load the scorelist
    [ContextMenu("Load")]
    public void LoadNow()
    {
        scoreList.Load(fullPath);
    }
}
