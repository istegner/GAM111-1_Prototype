﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public Rigidbody2D rb;

    public GameObject bulletSpawn;

    public float speed;

    public GameObject scoreManager;

    // Use this for initialization
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        Destroy(this.gameObject, 60.0f);
        rb.velocity = transform.up * speed;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Enemy" && this.gameObject.tag != "EnemyBullet")
        {
            Debug.Log("Hit");
            Destroy(collider.gameObject);
            scoreManager.GetComponent<ScoreManager>().AddScore(10);
            Destroy(this.gameObject);
        }
        else if (collider.gameObject.tag == "EnemyShoot" && this.gameObject.tag != "EnemyBullet")
        {
            Debug.Log("Hit");
            Destroy(collider.gameObject);
            scoreManager.GetComponent<ScoreManager>().AddScore(10);
            Destroy(this.gameObject);
        }
        else if (collider.gameObject.tag == "EnemyMelee" && this.gameObject.tag != "EnemyBullet")
        {
            Debug.Log("Hit");
            Destroy(collider.gameObject);
            scoreManager.GetComponent<ScoreManager>().AddScore(10);
            Destroy(this.gameObject);
        }
        else if (collider.gameObject.tag == "Wall")
        {
            Destroy(this.gameObject);
        }
    }
}
