﻿/// <PRIORITY>
/// Audio
/// Weapon types
/// Enemy types
/// Reflective journal
/// </PRIORITY>

/// <TODO>
/// 2 Pickups
/// Weapons
/// Enemy types
/// Scoring system
/// Instructions
/// Fix score system not updating player score
/// </TODO>

/// <Done>
/// 
/// </Done>


/// <Problems>
/// Add delay before enemy firing
/// </Problems>

/// <CREDITS>
/// Steve Halliwell
/// Tank and Healer Studio, Health Bar
/// OpenGameArt.org, Player and enemy sprites
/// </CREDITS>