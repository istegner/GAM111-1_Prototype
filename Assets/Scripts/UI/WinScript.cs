﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinScript : MonoBehaviour
{
    public string scoreListFileName;
    public string fullPath;

    public ScoreManager scoreManager;
    public InputField nameIF;
    public ScoreList scoreList;
    ScoreList.ScoreEntry scoreEntry = new ScoreList.ScoreEntry();

    // Use this for initialization
    void Start()
    {
        //The full path including the file
        fullPath = System.IO.Path.Combine(Application.persistentDataPath, scoreListFileName);
        scoreManager.GetComponent<ScoreManager>().LoadNow();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SaveHighscore()
    {
        scoreEntry.name = nameIF.text;
        scoreEntry.score = scoreManager.playerScore;
        scoreList.Save(fullPath);
    }
}
