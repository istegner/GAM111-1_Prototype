﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class ButtonManager : MonoBehaviour
{
    public Text gameNameText;
    public Text versionText;

    public Dropdown inputChoice;

    public bool useMouse;

    public string fullPath;
    public string fileName;

    // Use this for initialization
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            gameNameText.text = Application.productName;
            versionText.text = "V: " + Application.version;

            //The full path including the file
            fullPath = Path.Combine(Application.persistentDataPath, fileName);
            //Check if the file exists, if not, create it
            if (!File.Exists(fullPath))
            {
                File.Create(fullPath);
            }
            else
            {
                var option = File.ReadAllText(fullPath);
                if (option == 0.ToString())
                {
                    inputChoice.value = 0;
                }
                else
                {
                    inputChoice.value = 1;
                }
            }
        }
    }

    public void StartPressed()
    {
        SceneManager.LoadScene(1);
    }

    public void CreditsPressed()
    {
        Debug.Log("Credits");
    }

    public void ExitPressed()
    {
        Application.Quit();
        Debug.Log("Quit");
    }

    public void InputChange()
    {
        if (inputChoice.value == 0)
        {
            File.WriteAllText(fullPath, inputChoice.value.ToString());
            useMouse = true;
        }
        else
        {
            File.WriteAllText(fullPath, inputChoice.value.ToString());
            useMouse = false;
        }
        Debug.Log("Input Changed to " + useMouse);
    }

    public void MenuPressed()
    {
        SceneManager.LoadScene(0);
    }
}
