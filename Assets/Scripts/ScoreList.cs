﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


[System.Serializable]
public class ScoreList
{
    [System.Serializable]
    public struct ScoreEntry
    {
        public string name;
        public int score;
    }

    public List<ScoreEntry> entries = new List<ScoreEntry>();

    public void Save(string filePath)
    {
        var jsonified = JsonUtility.ToJson(this);
        System.IO.File.WriteAllText(filePath, jsonified);
        SortAndTrim();
    }

    public void Load(string filePath)
    {
        var fileContents = System.IO.File.ReadAllText(filePath);
        JsonUtility.FromJsonOverwrite(fileContents, this);
    }

    public void SortAndTrim()
    {
        entries = entries.OrderByDescending(x => x.score).Take(10).ToList();
    }
}
