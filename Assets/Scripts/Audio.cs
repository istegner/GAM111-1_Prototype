﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{

    public AudioSource audioSource;
    public AudioClip backgroundMusic;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(Play(backgroundMusic));
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Play(AudioClip sound)
    {
        audioSource.clip = sound;
        audioSource.Play();
        yield return new WaitForSeconds(sound.length);
        audioSource.Play();
    }
}
